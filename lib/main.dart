import 'package:flutter/material.dart';

import 'src/weather_app.dart';
import 'src/presentation/screen/settings/settings_provider.dart';
import 'src/presentation/screen/settings/settings_service.dart';

void main() async {
  final settingsController = SettingsProvider(SettingsService());
  await settingsController.loadSettings();
  runApp(WeatherApp(settingsController: settingsController));
}
