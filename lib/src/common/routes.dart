import 'package:flutter/material.dart';
import 'package:weather_app/src/presentation/screen/location/location_screen.dart';
import 'package:weather_app/src/presentation/screen/settings/settings_provider.dart';
import 'package:weather_app/src/presentation/screen/settings/settings_screen.dart';

class Routes {
  Routes._();

  static Map<String, WidgetBuilder> getRoutes(SettingsProvider settingsController) => {
    LocationScreen.routeName: (context) => const LocationScreen(),
    SettingsScreen.routeName: (context) => SettingsScreen(controller: settingsController),
  };
}
