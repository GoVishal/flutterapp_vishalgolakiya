import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:weather_app/src/common/constants.dart';
import 'package:weather_app/src/presentation/screen/widgets/no_internet_snack_bar.dart';

class ApiClient {

 static String getImageUrl(String name)
  => 'http://openweathermap.org/img/w/$name.png';

  Future<bool> _checkInternet() async{
    try {
      var result = await InternetAddress.lookup('google.com');

      if (result.isEmpty && result[0].rawAddress.isEmpty) {
        showNoInternetSnackBar();
        return false;
      }
    } catch (e) {
      showNoInternetSnackBar();
      return false;
    }
    return true;
  }

   Future<String?> getData(String location) async {
    if(await _checkInternet()) {
      String api = 'https://api.openweathermap.org/data/2.5/weather';
      String appId = '2c80e56db12a3f9eae9db33f83a2d449';

      String url = '$api?q=$location&APPID=$appId';
      print('url :: $url');
      http.Response response = await http.get(Uri.parse(url));

      if (response.body.isEmpty) {
        return 'Unable to find weather data, please try again';
      }
      return response.body;
    }
  }
}