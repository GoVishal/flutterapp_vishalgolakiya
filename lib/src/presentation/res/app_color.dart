import 'package:flutter/material.dart';

class AppColor {
  const AppColor._();

  static const Color primary = Color(0xff323232);
  static const Color secondary = Color(0xff767676);
  static const Color offWhite = Color(0xffF5F5F5);
  static const Color lightGray = Color(0xffebebeb);
  static const Color white = Color(0xffffffff);
  static const Color hintText = Color(0xff979797);
  static const Color categoryBackground = Color(0xffE3E3E4);
  static const Color yellow = Color(0xffFF7A00);
}
