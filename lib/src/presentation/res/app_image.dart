class AppImage {
  AppImage._();

  static const String _path = 'assets/';
  static const String gorilla = _path + 'gorilla.png';
  static const String whiteArrow = _path + 'white_arrow.svg';
  static const String splashBack = _path + 'splash_back.png';
  static const String eyeOpen = _path + 'eye_open.svg';
  static const String eyeClose = _path + 'eye_close.svg';
  static const String dropdownArrow = _path + 'dropdown_arrow.svg';
  static const String goArrow = _path + 'go_arrow.svg';
  static const String address = _path + 'address.svg';
  static const String edit = _path + 'edit.svg';

  static const String banner = _path + 'banner.png';
  static const String product = _path + 'product.png';

  static const String heart_white = _path + 'ic_heart_white.png';
  static const String cart = _path + 'cart.svg';
  static const String category = _path + 'category.png';
  static const String category1 = _path + 'category1.png';
  static const String category2 = _path + 'category2.png';
  static const String category3 = _path + 'category3.png';
  static const String back = _path + 'back.svg';
  static const String blackArrow = _path + 'black_arrow.png';
  static const String productDetailBack = _path + 'product_detail_back.png';

  // dashboard
  static const String user = _path + 'user.svg';
  static const String heart = _path + 'heart.svg';
  static const String lock = _path + 'lock.svg';
  static const String logout = _path + 'logout.svg';
  static const String shoppingCartTru = _path + 'shopping_cart.svg';
  static const String rightArrow = _path + 'right_arrow.svg';
  static const String camera = _path + 'camera.svg';
  static const String star = _path + 'star.png';

  static const String home  = _path + 'home.svg';
  static const String orders  = _path + 'order.svg';
  static const String ic_category  = _path + 'ic_category.svg';
  static const String profile  = _path + 'profile.svg';
  static const String contact  = _path + 'contact.svg';

  static const String pic  = _path + 'pic.png';

}
