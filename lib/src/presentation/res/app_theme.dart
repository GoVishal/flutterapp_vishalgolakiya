import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'app_color.dart';
import 'theme_text.dart';

class AppTheme {
  AppTheme._();

  static ThemeData lightTheme = ThemeData(
      primarySwatch: Colors.teal,
      primaryColor: AppColor.primary,
      textTheme: ThemeText.getTextTheme(),
      scaffoldBackgroundColor: AppColor.offWhite,
      bottomAppBarColor: AppColor.offWhite,
      inputDecorationTheme: InputDecorationTheme(
        filled: true,
        fillColor: AppColor.lightGray,
        hintStyle: GoogleFonts.poppinsTextTheme()
            .bodyText2!
            .copyWith(color: AppColor.hintText, fontSize: 14),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 14.0, horizontal: 14.0),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12.0),
            borderSide: BorderSide.none),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12.0),
            borderSide: BorderSide.none),
      ),
);

  static ThemeData darkTheme = ThemeData.dark().copyWith(
    textTheme: ThemeText.getTextTheme(),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: AppColor.categoryBackground,
      hintStyle: GoogleFonts.poppinsTextTheme()
          .bodyText2!
          .copyWith(color: AppColor.hintText, fontSize: 14),
      contentPadding:
      const EdgeInsets.symmetric(vertical: 14.0, horizontal: 14.0),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.0),
          borderSide: BorderSide.none),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.0),
          borderSide: BorderSide.none),
    ),
  );
}
