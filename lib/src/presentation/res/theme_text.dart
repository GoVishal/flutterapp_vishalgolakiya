import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'app_color.dart';

class ThemeText {
  const ThemeText._();

  static TextTheme get _poppinsTextTheme => GoogleFonts.poppinsTextTheme();

  static TextStyle get _headline5 => _poppinsTextTheme.headline5!.copyWith(
        fontSize: 26,
        fontWeight: FontWeight.bold,
        color: AppColor.primary,
      );

  static TextStyle get _buttonText => _poppinsTextTheme.button!
      .copyWith(color: AppColor.white, fontWeight: FontWeight.w600);

  static TextStyle get secondarySubtitle1 =>
      _poppinsTextTheme.subtitle1!.copyWith(
          color: AppColor.secondary, fontWeight: FontWeight.w400, fontSize: 14);

  static TextStyle get primaryBodyText2 =>
      _poppinsTextTheme.bodyText2!.copyWith(
        color: AppColor.primary,
      );

  static TextStyle get primaryCaption => _poppinsTextTheme.caption!.copyWith(
        fontSize: 10,
        color: AppColor.primary,
      );

  static getTextTheme() => TextTheme(
      headline5: _headline5,
      bodyText2: primaryBodyText2,
      subtitle1: secondarySubtitle1,
      button: _buttonText,
      caption: primaryCaption);
}

extension TextThemeExtension on TextTheme {
  TextStyle get hintTextStyle => subtitle2!.copyWith(
        color: AppColor.hintText,
      );

  TextStyle get formLabel => headline6!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w500, fontSize: 20.0);

  TextStyle get label_16_500 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w500, fontSize: 16);

  TextStyle get label_16_600 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w600, fontSize: 16);

  TextStyle get label_18_600 => subtitle1!.copyWith(
      color: AppColor.secondary, fontWeight: FontWeight.w600, fontSize: 18);

  TextStyle get primary_18_600 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w600, fontSize: 18);

  TextStyle get primary_18_500 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w500, fontSize: 18);

  TextStyle get primary_12_500 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w500, fontSize: 12);

  TextStyle get primary_12_400 => subtitle1!.copyWith(
      color: AppColor.primary, fontWeight: FontWeight.w400, fontSize: 12);
}
