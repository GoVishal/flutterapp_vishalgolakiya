import 'package:flutter/material.dart';
import 'package:weather_app/generated/l10n.dart';
import 'package:weather_app/src/data/data_source/api_client.dart';
import 'package:weather_app/src/data/models/weather_model.dart';

class WeatherView extends StatelessWidget {
  const WeatherView({
    Key? key,
    required this.model,
  }) : super(key: key);

  final WeatherModel model;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(model.name, style: Theme.of(context).textTheme.headline6),
        if (model.weather.isNotEmpty)
          Row(
            children: [
              SizedBox(
                  height: 52,
                  width: 52,
                  child: Image.network(ApiClient.getImageUrl(model.weather[0].icon))),
              Text(
                model.weather[0].main,
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ],
          ),
        _itemTile(context, S.current.tempressure,
            '${(model.main.temp - 273.15).toStringAsFixed(2)}°'),
        _itemTile(context, S.current.wind, '${model.wind.speed} meter/sec'),
        _itemTile(context, S.current.pressure, '${model.main.pressure} hPa'),
      ],
    );
  }

  _itemTile(BuildContext context, String label, String data) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        Text(
          data,
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ],
    );
  }
}

//
