import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/generated/l10n.dart';
import 'package:weather_app/src/common/constants.dart';
import 'package:weather_app/src/data/data_source/api_client.dart';
import 'package:weather_app/src/data/models/weather_model.dart';
import 'package:weather_app/src/presentation/screen/widgets/no_internet_snack_bar.dart';

class LocationProvider extends ChangeNotifier {
  bool isCalled = false;
  TextEditingController locationController = TextEditingController();
  WeatherModel? weatherData;
  bool isLoading = false;

  void init() async {
    if(!isCalled){
      isCalled = true;
      weatherData = await getPreviousLocation();
      if(weatherData != null){
        notifyListeners();
      }
    }
  }

  void getData() async {
    if (locationController.text.trim().isEmpty) {
      showAppSnackBar(S.current.err_enter_location);
      notifyListeners();
      return;
    }

    isLoading = true;
    notifyListeners();
    final resp = await ApiClient().getData(locationController.text);

    if(resp != null) {
      final temp = json.decode(resp);
      if(temp['cod'] == 200) {
        weatherData = WeatherModel.fromJson(temp);
        storeSearch(weatherData!);
      } else {
        showAppSnackBar(temp['message']??'Enable to fetch data from server');
      }
    }
    isLoading = false;
    notifyListeners();
  }

  void storeSearch(WeatherModel model) async {
    final prefs = await SharedPreferences.getInstance();

    prefs.setString('previous_location', jsonEncode(model.toJson()));
  }

  Future<WeatherModel?> getPreviousLocation() async {
    final prefs = await SharedPreferences.getInstance();
    final str = prefs.getString('previous_location');
    if(str != null) {
      return WeatherModel.fromJson(jsonDecode(str));
    }
    return null;
  }
}
