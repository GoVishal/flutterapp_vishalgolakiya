import 'package:flutter/material.dart';
import 'package:weather_app/generated/l10n.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/src/common/constants.dart';
import 'package:weather_app/src/presentation/screen/location/location_provider.dart';
import 'package:weather_app/src/presentation/screen/widgets/custom_button.dart';

import '../settings/settings_screen.dart';
import 'components/weather_view.dart';

/// Displays a text field to take a location name from user
/// Show weather data below the text field

class LocationScreen extends StatelessWidget {
  const LocationScreen({
    Key? key,
  }) : super(key: key);

  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    context.read<LocationProvider>().init();

    return Scaffold(
      appBar: AppBar(
        title: Text(S.current.appTitle),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.restorablePushNamed(context, SettingsScreen.routeName);
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              S.current.enter_location,
              style: Theme.of(context).textTheme.subtitle2,
            ),
            const SizedBox(height: defaultPadding),
            _locationField(context),
            const SizedBox(height: defaultPadding),
            CustomButton(
                text: S.current.get_weather,
                onTap: context.read<LocationProvider>().getData),
            if (context.watch<LocationProvider>().weatherData != null)
              Padding(
                padding: const EdgeInsets.all(defaultPadding),
                child: WeatherView(
                    model: (context.watch<LocationProvider>().weatherData!)),
              ),
          ],
        ),
      ),
    );
  }

  Widget _locationField(BuildContext context) =>
      TextField(
        style: Theme.of(context).textTheme.bodyText2,
        controller: context.read<LocationProvider>().locationController,
        onSubmitted: (_) => context.read<LocationProvider>().getData(),
        decoration: InputDecoration(
            suffix: context.watch<LocationProvider>().isLoading
                ? const SizedBox(
                height: defaultPadding,
                width: defaultPadding,
                child: CircularProgressIndicator(
                  strokeWidth: 1,
                ))
                : null),
      );
}
