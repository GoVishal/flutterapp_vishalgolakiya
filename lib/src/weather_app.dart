import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/generated/l10n.dart';
import 'package:weather_app/src/common/constants.dart';
import 'package:weather_app/src/presentation/res/app_theme.dart';
import 'package:weather_app/src/presentation/screen/location/location_provider.dart';

import 'common/routes.dart';
import 'presentation/screen/location/location_screen.dart';
import 'presentation/screen/settings/settings_provider.dart';

/// The Widget that configures your application.
class WeatherApp extends StatelessWidget {
  const WeatherApp({
    Key? key,
    required this.settingsController,
  }) : super(key: key);

  final SettingsProvider settingsController;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: settingsController,
      builder: (BuildContext context, Widget? child) {
        return MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => LocationProvider()),
          ],
          child: MaterialApp(
            restorationScopeId: 'app',
            navigatorKey: navigatorKey,
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [
              Locale('en', ''), // English, no country code
            ],

            onGenerateTitle: (BuildContext context) =>
                S.of(context).appTitle,
            theme: AppTheme.lightTheme,
            darkTheme: AppTheme.darkTheme,
            themeMode: settingsController.themeMode,
            initialRoute: LocationScreen.routeName,
            routes: Routes.getRoutes(settingsController),
          ),
        );
      },
    );
  }
}
